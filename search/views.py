from django.http.response import HttpResponse
from django.shortcuts import render
from SPARQLWrapper import SPARQLWrapper, JSON

def index(request):
    if request.method == "POST":
        input = {}
        input['category'] = request.POST.get('category')
        input['input-a'] = request.POST.get('input-a')

        if input['category'] == 'Country':
            results = search_country(input['input-a'])
            return render(request, 'main.html', {'results': results, 'category': input['category']})
        elif input['category'] == 'League':
            results = search_league(input['input-a'])
            return render(request, 'main.html', {'results': results, 'category': input['category']})
        elif input['category'] == 'Team':
            results = search_team(input['input-a'])
            return render(request, 'main.html', {'results': results, 'category': input['category']})
        elif input['category'] == 'Player':
            results = search_player(input['input-a'])
            return render(request, 'main.html', {'results': results, 'category': input['category']})

    return render(request, 'main.html')  
    
def search_country(input):
    searchText = input
    print(searchText)
    sparql = SPARQLWrapper('http://localhost:9999/blazegraph/sparql')
    sparql.setQuery('''
        PREFIX o: <http://example.org/ontology/>
        PREFIX p: <http://example.org/property/>

        SELECT ?country ?countryName
        WHERE {
            ?country a o:country .
            ?country p:countryName ?countryName .
            FILTER (
                (REGEX(?countryName, ".*''' + searchText + '''.*", "i"))
            )
        }
    ''')
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results['results']['bindings']:
        print(result['country']['value'])
        print('''Country: ''' + result['country']['value'] + '\n' + 'Country Name: ' + result['countryName']['value'] + '\n\n')

    return results

def search_league(input):
    searchText = input
    print(searchText)
    sparql = SPARQLWrapper('http://localhost:9999/blazegraph/sparql')
    sparql.setQuery('''
        PREFIX o: <http://example.org/ontology/>
        PREFIX p: <http://example.org/property/>

        SELECT ?league ?leagueName ?country ?countryName
        WHERE {
            ?league a o:league .
            ?league p:leagueName ?leagueName .
            ?league p:isLeagueOfCountry ?country .
            ?country p:countryName ?countryName .
            FILTER (
                (REGEX(?leagueName, ".*''' + searchText + '''.*", "i")) ||
                (REGEX(?countryName, ".*''' + searchText + '''.*", "i"))
            )
        }
    ''')
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results['results']['bindings']:
        print('League: ' + result['league']['value'] + '\n' + 'League Name: ' + result['leagueName']['value'] + '\n' + 'Country: ' + result['country']['value'] + '\n' + 'Country Name: ' + result['countryName']['value'] + '\n\n')

    return results

def search_player(input):
    searchText = input
    print(searchText)
    sparql = SPARQLWrapper('http://localhost:9999/blazegraph/sparql')
    sparql.setQuery('''
        PREFIX o: <http://example.org/ontology/>
        PREFIX p: <http://example.org/property/>

        SELECT ?player ?playerName ?birthday ?height ?weight
        WHERE {
            ?player a o:player .
            ?player p:playerName ?playerName .
            ?player p:birthday ?birthday .
            ?player p:height ?height .
            ?player p:weight ?weight .
            FILTER (
                (REGEX(?playerName, ".*''' + searchText + '''.*", "i"))
            )
        }
    ''')
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    playerListTxt = ''
    for result in results['results']['bindings']:
        playerListTxt += 'Player: ' + result['player']['value'] + '\n' + 'Player Name: ' + result['playerName']['value'] + '\n' + 'Birthday: ' + result['birthday']['value'][:10] + '\n' + 'Height (cm): ' + result['height']['value'] + '\n' + 'Weight (lbs): ' + result['weight']['value'] + '\n\n'
        result['birthday']['value'] = result['birthday']['value'][:10]
        result['player']['value'] = result['player']['value'][35:]

    return results

def search_team(input):
    searchText = input
    print(searchText)
    sparql = SPARQLWrapper('http://localhost:9999/blazegraph/sparql')
    sparql.setQuery('''
        PREFIX o: <http://example.org/ontology/>
        PREFIX p: <http://example.org/property/>

        SELECT ?team ?teamLongName ?teamShortName
        WHERE {
            ?team a o:team .
            ?team p:teamLongName ?teamLongName .
            ?team p:teamShortName ?teamShortName .
            FILTER (
                (REGEX(?teamLongName, ".*''' + searchText + '''.*", "i")) ||
                (REGEX(?teamShortName, ".*''' + searchText + '''.*", "i"))
            )
        }
    ''')
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    playerListTxt = ''
    for result in results['results']['bindings']:
        playerListTxt += 'Team: ' + result['team']['value'] + '\n' + 'Team Long Name: ' + result['teamLongName']['value'] + '\n' + 'Team Short Name: ' + result['teamShortName']['value'] + '\n\n'
        result['team']['value'] = result['team']['value'][33:]

    return results

def player_detail(request, id):
    sparql = SPARQLWrapper('http://localhost:9999/blazegraph/sparql')

    sparql.setQuery('''
        PREFIX o: <http://example.org/ontology/>
        PREFIX p: <http://example.org/property/>

        PREFIX r_player: <http://example.org/resource/player/>

        SELECT ?playerName ?birthday ?height ?weight ?overallRating ?dateRated
        WHERE {
            r_player:''' + id + ''' p:playerName ?playerName .
            r_player:''' + id + ''' p:birthday ?birthday .
            r_player:''' + id + ''' p:height ?height .
            r_player:''' + id + ''' p:weight ?weight .
            OPTIONAL {
                {
                    SELECT ?playerAttribute ?dateRated
                    WHERE {
                        ?playerAttribute p:isAttributeOfPlayer r_player:''' + id + ''' .
                        ?playerAttribute p:isPlayerAttributeOfDate ?dateRated .
                    }
                    ORDER BY DESC (?dateRated) LIMIT 1
                }
                ?playerAttribute p:overallRating ?overallRating .
            }
        }
        LIMIT 1
    ''')
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    result = results['results']['bindings'][0]

    attText = ''
    try:
        attText += '\n\nLatest FIFA Overall Rating: ' + result['overallRating']['value'] + '\n'
        attText += 'Date Rated: ' + result['dateRated']['value'][:10]
    except:
        print('FIFA Player Attribute not found')
        attText = '\n\nFIFA Player Attribute not found'

    playerDbLink = result['playerName']['value'].replace(' ', '_')
    dbSparql = SPARQLWrapper('http://dbpedia.org/sparql')

    dbSparql.setQuery('''
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbo: <http://dbpedia.org/ontology/>

        SELECT ?abstract ?thumbnail
        WHERE {
            dbr:''' + playerDbLink + ''' dbo:abstract ?abstract .
            dbr:''' + playerDbLink + ''' dbo:thumbnail ?thumbnail .
            FILTER langMatches(lang(?abstract), "en")
        }
    ''')
    dbSparql.setReturnFormat(JSON)
    dbResults = dbSparql.query().convert()

    dbText = ''
    try:
        dbResult = dbResults['results']['bindings'][0]
        dbText += '\n\nDBPedia Link: http://dbpedia.org/resource/' + playerDbLink + '\n' + 'Description:\n' + dbResult['abstract']['value'] + '\n\n' + 'Image: ' + dbResult['thumbnail']['value']
    except:
        print("DBPedia link not found")
        dbText = '\n\nDBPedia link not found'

    playerTxt = 'Player: http://example.org/resource/player/' + id + '\n' + 'Player Name: ' + result['playerName']['value'] + '\n\n' + 'Birthday: ' + result['birthday']['value'][:10] + '\n' + 'Height (cm): ' + result['height']['value'] + '\n' + 'Weight (lbs): ' + result['weight']['value'] + attText + dbText
    # return render(request, 'player_detail.html',{})
    return HttpResponse(playerTxt, content_type='text/plain')

def team_detail(request, id):
    sparql = SPARQLWrapper('http://localhost:9999/blazegraph/sparql')

    sparql.setQuery('''
        PREFIX o: <http://example.org/ontology/>
        PREFIX p: <http://example.org/property/>

        PREFIX r_team: <http://example.org/resource/team/>

        SELECT
            ?teamLongName
            ?teamShortName
            ?buildUpPlaySpeed
            ?buildUpPlayDribbling
            ?buildUpPlayPassing
            ?buildUpPlayPositioning
            ?chanceCreationPassing
            ?chanceCreationCrossing
            ?chanceCreationShooting
            ?chanceCreationPositioning
            ?defencePressure
            ?defenceAggression
            ?defenceTeamWidth
            ?defenceDefenderLine
            ?dateRated
        WHERE {
            r_team:''' + id + ''' p:teamLongName ?teamLongName .
            r_team:''' + id + ''' p:teamShortName ?teamShortName .
            OPTIONAL {
                {
                    SELECT ?teamAttribute ?dateRated
                    WHERE {
                        ?teamAttribute p:isAttributeOfTeam r_team:''' + id + ''' .
                        ?teamAttribute p:isTeamAttributeOfDate ?dateRated .
                    }
                    ORDER BY DESC (?dateRated) LIMIT 1
                }
                ?teamAttribute p:buildUpPlaySpeed ?buildUpPlaySpeed .
                ?teamAttribute p:buildUpPlayDribbling ?buildUpPlayDribbling .
                ?teamAttribute p:buildUpPlayPassing ?buildUpPlayPassing .
                ?teamAttribute p:buildUpPlayPositioning ?buildUpPlayPositioning .
                ?teamAttribute p:chanceCreationPassing ?chanceCreationPassing .
                ?teamAttribute p:chanceCreationCrossing ?chanceCreationCrossing .
                ?teamAttribute p:chanceCreationShooting ?chanceCreationShooting .
                ?teamAttribute p:chanceCreationPositioning ?chanceCreationPositioning .
                ?teamAttribute p:defencePressure ?defencePressure .
                ?teamAttribute p:defenceAggression ?defenceAggression .
                ?teamAttribute p:defenceTeamWidth ?defenceTeamWidth .
                ?teamAttribute p:defenceDefenderLine ?defenceDefenderLine .
            }
        }
        LIMIT 1
    ''')
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    result = results['results']['bindings'][0]

    attText = ''
    try:
        attText += '\n\nLatest FIFA Rating - Build Up Play Speed          : ' + result['buildUpPlaySpeed']['value'] + '\n'
        attText += 'Latest FIFA Rating - Build Up Play Dribbling      : ' + result['buildUpPlayDribbling']['value'] + '\n'
        attText += 'Latest FIFA Rating - Build Up Play Passing        : ' + result['buildUpPlayPassing']['value'] + '\n'
        attText += 'Latest FIFA Rating - Build Up Play Positioning    : ' + result['buildUpPlayPositioning']['value'] + '\n'
        attText += 'Latest FIFA Rating - Chance Creation Passing      : ' + result['chanceCreationPassing']['value'] + '\n'
        attText += 'Latest FIFA Rating - Chance Creation Crossing     : ' + result['chanceCreationCrossing']['value'] + '\n'
        attText += 'Latest FIFA Rating - Chance Creation Shooting     : ' + result['chanceCreationShooting']['value'] + '\n'
        attText += 'Latest FIFA Rating - Chance Creation Positioning  : ' + result['chanceCreationPositioning']['value'] + '\n'
        attText += 'Latest FIFA Rating - Defence Pressure             : ' + result['defencePressure']['value'] + '\n'
        attText += 'Latest FIFA Rating - Defence Aggression           : ' + result['defenceAggression']['value'] + '\n'
        attText += 'Latest FIFA Rating - Defence Team Width           : ' + result['defenceTeamWidth']['value'] + '\n'
        attText += 'Latest FIFA Rating - Defence Defender Line        : ' + result['defenceDefenderLine']['value'] + '\n'
        attText += 'Date Rated: ' + result['dateRated']['value'][:10]
    except:
        print('FIFA Team Attribute not found')
        attText = '\nFIFA Team Attribute not found'

    teamDbLink = result['teamLongName']['value'].replace(' ', '_')
    dbSparql = SPARQLWrapper('http://dbpedia.org/sparql')

    dbSparql.setQuery('''
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbo: <http://dbpedia.org/ontology/>

        SELECT ?abstract
        WHERE {
            dbr:''' + teamDbLink + ''' dbo:abstract ?abstract .
            FILTER langMatches(lang(?abstract), "en")
        }
    ''')
    dbSparql.setReturnFormat(JSON)
    dbResults = dbSparql.query().convert()

    dbText = ''
    try:
        dbResult = dbResults['results']['bindings'][0]
        dbText = '\n\nDBPedia Link: http://dbpedia.org/resource/' + teamDbLink + '\n\n' + 'Description:\n' + dbResult['abstract']['value']
    except:
        print("DBPedia link not found")
        dbText = '\n\nDBPedia link not found'

    teamTxt = 'Team: http://example.org/resource/team/' + id + '\n' + 'Team Long Name: ' + result['teamLongName']['value'] + '\n' + 'Team Short Name: ' + result['teamShortName']['value'] + attText + dbText
    # return render(request, 'team_detail.html',{})
    return HttpResponse(teamTxt, content_type='text/plain')