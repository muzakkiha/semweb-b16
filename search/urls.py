from django.urls import path
from . import views

app_name = "search"

urlpatterns = [
    path('', views.index, name='index'),
    path('player_detail/<str:id>', views.player_detail, name='player_detail'),
    path('team_detail/<str:id>', views.team_detail, name='team_detail'),
]