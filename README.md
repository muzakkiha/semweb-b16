# EUROPE bOWLa

## Setup App

1.  Make virtual environment

    ```shell
    py -m venv env
    ```

2.  Run virtual environment

    ```shell
    env\Scripts\activate
    ```

3.  Install dependencies

    ```shell
    py -m pip install -r requirements.txt
    ```

4.  Migrate & collect static

    ```shell
    py manage.py migrate
    py manage.py collectstatic
    ```

5.  Run server

    ```shell
    py manage.py runserver
    ```

6.  Run makemigrations after changing models

    ```shell
    py manage.py makemigrations
    ```

## Setup Dataset & Triple Store

1.  Download, install, & run Blazegraph from https://github.com/blazegraph/database/wiki/Quick_Start

2.  Extract dataset.zip

3.  Load dataset.ttl to Blazegraph: 
    - In Blazegraph webpage, go to UPDATE tab
    - Click "Choose File" > dataset.ttl (or drag & drop dataset.ttl to the input box)
    - Click "Update"